package cn.ijero.composeui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.page.advanced.AdvancedActivity
import cn.ijero.composeui.page.basic.BasicActivity
import cn.ijero.composeui.tool.VerticalSpacer
import cn.ijero.composeui.ui.theme.ComposeUITheme

class MainActivity : BaseActivity() {

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {
    val context = LocalContext.current
    Column(modifier = Modifier.padding(16.dp)) {
        Button(modifier = Modifier
            .fillMaxWidth(),
            onClick = {
                BasicActivity.start(context)
            }
        ) {
            Text(text = "基础组件")
        }
        VerticalSpacer()
        Button(modifier = Modifier
            .fillMaxWidth(),
            onClick = {
                AdvancedActivity.start(context)
            }
        ) {
            Text(text = "进阶组件")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeUITheme {
        RootLayout()
    }
}