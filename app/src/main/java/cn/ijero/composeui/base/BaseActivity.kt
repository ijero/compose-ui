package cn.ijero.composeui.base

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import cn.ijero.composeui.ui.theme.ComposeUITheme


abstract class BaseActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeUITheme {
                Surface(color = MaterialTheme.colors.background) {
                    RenderContent()
                }
            }
        }
    }

    @Composable
    abstract fun RenderContent()

}