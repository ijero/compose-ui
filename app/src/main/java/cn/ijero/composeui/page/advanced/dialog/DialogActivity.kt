package cn.ijero.composeui.page.advanced.dialog

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.R
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.tool.VerticalSpacer

class DialogActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, DialogActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {
    Column {
        CardCompose()
        VerticalSpacer()
    }
}

@Composable
private fun CardCompose() {
    var openDialog by remember {
        mutableStateOf(false)
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp),
        elevation = 6.dp
    ) {
        Column {
            Image(
                painter = painterResource(id = R.mipmap.img),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(240.dp),
                contentScale = ContentScale.FillWidth,
                contentDescription = null
            )
            Text(
                text = stringResource(R.string.text_dandelion),
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier.padding(12.dp, 6.dp)
            )
            Text(
                text = stringResource(R.string.text_dandelion_info),
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(12.dp, 0.dp),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
            VerticalSpacer()

            TextButton(
                modifier = Modifier
                    .align(Alignment.End)
                    .padding(12.dp, 0.dp),
                onClick = {
                    openDialog = true
                },
            ) {
                Text(text = "查看详情")
            }
        }
    }

    if (openDialog) {
        AlertDialog(
            title = {
                Text(text = stringResource(id = R.string.text_dandelion))
            },
            text = {
                Text(text = stringResource(id = R.string.text_dandelion_info))
            },
            onDismissRequest = {
                openDialog = false
            },
            confirmButton = {
                TextButton(onClick = {
                    openDialog = false
                }) {
                    Text(text = "确定")
                }
            },
            dismissButton = {
                TextButton(onClick = {
                    openDialog = false
                }) {
                    Text(text = "取消")
                }
            }
        )
    }

}


@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    RootLayout()
}