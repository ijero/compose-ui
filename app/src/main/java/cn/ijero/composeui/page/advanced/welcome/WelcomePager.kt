package cn.ijero.composeui.page.advanced.welcome

import androidx.compose.foundation.Image
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import cn.ijero.composeui.R
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun WelcomePager(
    items: List<String>,
    state: PagerState
) {
    HorizontalPager(count = items.size, state = state) { page ->
        ConstraintLayout {
            val (image, text) = createRefs()
            Image(
                modifier = Modifier.constrainAs(image) {
                    centerHorizontallyTo(parent)
                    top.linkTo(parent.top)
                    bottom.linkTo(text.top)
                },
                painter = painterResource(id = R.mipmap.img),
                contentDescription = null
            )
            Text(modifier = Modifier.constrainAs(text) {
                centerHorizontallyTo(parent)
                bottom.linkTo(parent.bottom)
                top.linkTo(image.bottom, 12.dp)
            }, text = "第 ${page + 1} 页 : ${items[page]}", style = MaterialTheme.typography.h5)
        }
    }
}