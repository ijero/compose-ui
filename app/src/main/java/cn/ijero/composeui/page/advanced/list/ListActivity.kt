package cn.ijero.composeui.page.advanced.list

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.R
import cn.ijero.composeui.base.BaseActivity

class ListActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, ListActivity::class.java)
            context.startActivity(starter)
        }
    }

    @ExperimentalMaterialApi
    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

data class ItemData(
    val title: String,
    val content: String
)

@ExperimentalMaterialApi
@Composable
private fun RootLayout() {
    val list = listOf(
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
        ItemData("Android", "content1"),
        ItemData("IOS", "content2"),
        ItemData("Windows", "content3"),
    )

    LazyColumn(
        Modifier.background(MaterialTheme.colors.secondary),
        contentPadding = PaddingValues(12.dp),
    ) {
        items(list) { item ->
            MyListItem(item)
        }
    }
}

@ExperimentalMaterialApi
@Composable
private fun MyListItem(item: ItemData) {
    ListItem(
        modifier = Modifier.background(color = Color.White),
        icon = {
            Image(
                painter = painterResource(id = R.mipmap.img),
                contentDescription = null,
                modifier = Modifier.size(80.dp),
            )
        },
        text = {
            Text(text = item.title)
        },
        secondaryText = {
            Text(text = item.content)
        }
    )
    Divider(color = Color.Red, modifier = Modifier.padding(12.dp, 0.dp))
}

@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    RootLayout()
}