package cn.ijero.composeui.page.advanced.welcome

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import cn.ijero.composeui.base.BaseActivity
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState

class WelcomeActivity : BaseActivity() {
    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, WelcomeActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@OptIn(ExperimentalPagerApi::class)
@Composable
private fun RootLayout() {
    val items = listOf("Android", "IOS", "Windows", "MacOS")
    val state = rememberPagerState(0)
    val context = LocalContext.current
    ConstraintLayout {
        val (indicator, button) = createRefs()
        WelcomePager(items, state = state)
        Indicator(
            size = items.size,
            index = state.currentPage,
            modifier = Modifier.constrainAs(indicator) {
                centerHorizontallyTo(parent)
                bottom.linkTo(parent.bottom, 24.dp)
            })

        val width =
            animateDpAsState(
                targetValue = if (state.currentPage.inc() == items.size) 120.dp else 0.dp,
                animationSpec = spring(Spring.DampingRatioMediumBouncy)
            )
        Button(modifier = Modifier
            .height(40.dp)
            .width(width.value)
            .constrainAs(button) {
                bottom.linkTo(indicator.top, 24.dp)
                centerHorizontallyTo(parent)
            }, onClick = {
            Toast.makeText(context, "点击了进入首页", Toast.LENGTH_SHORT).show()
        }) {
            Text(text = "进入首页", fontSize = 12.sp)
        }
    }
}

@Composable
fun Indicator(size: Int, index: Int, modifier: Modifier) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        repeat(size) {
            HorizontalIndicator(isSelected = it == index)
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    RootLayout()
}
