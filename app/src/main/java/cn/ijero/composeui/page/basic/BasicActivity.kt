package cn.ijero.composeui.page.basic

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.tool.VerticalSpacer

class BasicActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, BasicActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()
    Scaffold(scaffoldState = scaffoldState) {
        Column(modifier = Modifier.padding(16.dp)) {
            // text
            TextCompose()
            VerticalSpacer()
            // button
            ButtonCompose(coroutineScope, scaffoldState)
            VerticalSpacer()
            // image
            ImageCompose()
            VerticalSpacer()
            // checkbox
            CheckBoxCompose()
            VerticalSpacer()
            // radiobutton
            RadioButtonCompose()
            VerticalSpacer()
            // switch
            SwitchCompose()
            VerticalSpacer()
            // floatingActionButton
            FloatingActionButtonCompose(coroutineScope, scaffoldState)
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    RootLayout()
}