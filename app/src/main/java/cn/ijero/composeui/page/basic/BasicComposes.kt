package cn.ijero.composeui.page.basic

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.R
import cn.ijero.composeui.tool.HorizontalSpacer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Composable
fun FloatingActionButtonCompose(
    coroutineScope: CoroutineScope,
    scaffoldState: ScaffoldState
) {
    FloatingActionButton(onClick = {
        coroutineScope.launch {
            scaffoldState.snackbarHostState.showSnackbar("点击了添加按钮")
        }
    }) {
        Icon(
            painter = rememberVectorPainter(image = Icons.Filled.Add),
            contentDescription = null
        )
    }
}

@Composable
fun SwitchCompose() {
    var isCheckedSwitch by remember {
        mutableStateOf(false)
    }
    Switch(checked = isCheckedSwitch, onCheckedChange = {
        isCheckedSwitch = !isCheckedSwitch
    })
}

@Composable
fun RadioButtonCompose() {
    val tags = mutableListOf("Android", "Kotlin", "Compose")
    var selectedTag by remember {
        mutableStateOf("")
    }
    Row(verticalAlignment = Alignment.CenterVertically) {
        for (tag in tags) {
            RadioButton(selected = selectedTag == tag, onClick = {
                selectedTag = tag
            })
            Text(text = tag, style = MaterialTheme.typography.body2)
        }
    }
}

@Composable
fun TextCompose() {
    Text(text = "这是body1文本", style = MaterialTheme.typography.body1)
    Text(text = "这是body2文本", style = MaterialTheme.typography.body2)
}

@Composable
fun ButtonCompose(
    coroutineScope: CoroutineScope,
    scaffoldState: ScaffoldState
) {
    var buttonEnabled by remember {
        mutableStateOf(true)
    }

    Button(
        onClick = {
            buttonEnabled = false
            coroutineScope.launch {
                scaffoldState.snackbarHostState.showSnackbar("点击了按钮，已设置为禁用状态", "我知道了")
            }
        }, enabled = buttonEnabled
    ) {
        Text(text = "这是一个按钮")
    }
}

@Composable
fun ImageCompose() {
    Row {
        Image(
            painter = painterResource(id = R.mipmap.img),
            contentDescription = null,
            modifier = Modifier.size(64.dp)
        )
        HorizontalSpacer()
        Image(
            painter = painterResource(id = R.mipmap.img),
            contentDescription = null,
            modifier = Modifier
                .size(64.dp)
                .clip(CircleShape)
        )
        HorizontalSpacer()
        Image(
            painter = painterResource(id = R.mipmap.img),
            contentDescription = null,
            modifier = Modifier
                .size(64.dp)
                .clip(RoundedCornerShape(10.dp))
        )
        HorizontalSpacer()
        Image(
            painter = painterResource(id = R.mipmap.img),
            contentDescription = null,
            modifier = Modifier
                .size(64.dp)
                .clip(RoundedCornerShape(10.dp))
                .border(
                    1.5.dp,
                    MaterialTheme.colors.secondaryVariant,
                    RoundedCornerShape(10.dp)
                )
        )
    }
}

@Composable
fun CheckBoxCompose() {
    Row(verticalAlignment = Alignment.CenterVertically) {
        var checkboxChecked by remember {
            mutableStateOf(false)
        }
        var triState by remember {
            mutableStateOf(ToggleableState.Off)
        }
        Checkbox(
            checked = checkboxChecked,
            onCheckedChange = { checkboxChecked = !checkboxChecked },
        )
        Text(text = "CheckBox")
        HorizontalSpacer()
        TriStateCheckbox(state = triState, onClick = {
            triState = when (triState) {
                ToggleableState.Off -> {
                    ToggleableState.Indeterminate
                }
                ToggleableState.Indeterminate -> {
                    ToggleableState.On
                }
                else -> {
                    ToggleableState.Off
                }
            }
        })
        Text(text = "TriStateCheckbox")
    }
}