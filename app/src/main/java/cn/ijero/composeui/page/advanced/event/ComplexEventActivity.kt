package cn.ijero.composeui.page.advanced.event

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.tool.VerticalSpacer
import kotlin.math.roundToInt

class ComplexEventActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, ComplexEventActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp, 0.dp)
    ) {
        VerticalSpacer()
        Text(text = "单击、双击、长按：")
        VerticalSpacer()
        Box(modifier = Modifier
            .background(Color.Red)
            .fillMaxWidth()
            .height(45.dp)
            .pointerInput(Unit) {
                detectTapGestures(
                    onPress = { offset ->
                        // 所有事件触发前回调
                        Log.d("BasicEventActivity", "RootLayout: 触发了onPress事件，位置：$offset")
//                        Toast
//                            .makeText(context, "触发了onPress事件：$offset", Toast.LENGTH_SHORT)
//                            .show()
                    },
                    onTap = {
                        Toast
                            .makeText(context, "触发了单击，位置：$it", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDoubleTap = {
                        Toast
                            .makeText(context, "触发了双击，位置：$it", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onLongPress = {
                        Toast
                            .makeText(context, "触发了长按，位置：$it", Toast.LENGTH_SHORT)
                            .show()
                    }
                )
            }) {
            Text(text = "操作区域", modifier = Modifier.align(Alignment.Center))
        }
        VerticalSpacer()
        Text(text = "自由拖动：")
        VerticalSpacer()
        var offsetX by remember { mutableStateOf(0f) }
        var offsetY by remember { mutableStateOf(0f) }
        Box(modifier = Modifier
            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
            .background(Color.Green)
            .size(45.dp)
            .pointerInput(Unit) {
                detectDragGestures(
                    onDragStart = {
                        Toast
                            .makeText(context, "拖动开始，位置：$it", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDragEnd = {
                        Toast
                            .makeText(context, "拖动结束", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDragCancel = {
                        Toast
                            .makeText(context, "拖动取消", Toast.LENGTH_SHORT)
                            .show()
                    }
                ) { change, dragAmount ->
                    change.consumeAllChanges()
                    offsetX += dragAmount.x
                    offsetY += dragAmount.y
                }
            }) {
        }
    }
}