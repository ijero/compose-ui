package cn.ijero.composeui.page.advanced

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.page.advanced.banner.BannerActivity
import cn.ijero.composeui.page.advanced.custom.CanvasActivity
import cn.ijero.composeui.page.advanced.dialog.DialogActivity
import cn.ijero.composeui.page.advanced.event.BasicEventActivity
import cn.ijero.composeui.page.advanced.event.ComplexEventActivity
import cn.ijero.composeui.page.advanced.list.ListActivity
import cn.ijero.composeui.page.advanced.tabpager.TabPagerActivity
import cn.ijero.composeui.page.advanced.welcome.WelcomeActivity
import cn.ijero.composeui.tool.VerticalSpacer

class AdvancedActivity : BaseActivity() {
    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, AdvancedActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {

    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(scaffoldState = scaffoldState) {
        Column(modifier = Modifier.padding(16.dp)) {
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    DialogActivity.start(context)
                }
            ) {
                Text(text = "Card and Dialog")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    ListActivity.start(context)
                }
            ) {
                Text(text = "List")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    TabPagerActivity.start(context)
                }
            ) {
                Text(text = "Tab pager")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    WelcomeActivity.start(context)
                }
            ) {
                Text(text = "Welcome pager")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    CanvasActivity.start(context)
                }
            ) {
                Text(text = "Compose canvas")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    BannerActivity.start(context)
                }
            ) {
                Text(text = "Banner")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    BasicEventActivity.start(context)
                }
            ) {
                Text(text = "Basic events")
            }
            VerticalSpacer()
            Button(modifier = Modifier
                .fillMaxWidth(),
                onClick = {
                    ComplexEventActivity.start(context)
                }
            ) {
                Text(text = "Complex events")
            }
        }

    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    RootLayout()
}