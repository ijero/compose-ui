package cn.ijero.composeui.page.advanced.event

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import cn.ijero.composeui.base.BaseActivity
import cn.ijero.composeui.tool.VerticalSpacer
import kotlin.math.roundToInt

class BasicEventActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, BasicEventActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Composable
    override fun RenderContent() {
        RootLayout()
    }

}

@Composable
private fun RootLayout() {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp, 0.dp)
    ) {
        VerticalSpacer()
        Text(text = "单击事件：")
        VerticalSpacer()
        Box(modifier = Modifier
            .background(Color.Red)
            .fillMaxWidth()
            .height(45.dp)
            .clickable {
                Toast
                    .makeText(context, "触发了单击事件", Toast.LENGTH_SHORT)
                    .show()
            }) {
            Text(text = "单击我", modifier = Modifier.align(Alignment.Center))
        }
        VerticalSpacer()
        Text(text = "水平拖动事件：")
        VerticalSpacer()
        var offsetX by remember { mutableStateOf(0f) }
        Box(
            modifier = Modifier
                .offset { IntOffset(offsetX.roundToInt(), 0) }
                .background(Color.Blue)
                .size(45.dp)
                .draggable(
                    state = rememberDraggableState(
                        onDelta = {
                            offsetX += it
                        }
                    ),
                    orientation = Orientation.Horizontal,
                    onDragStarted = { offset ->
                        Toast
                            .makeText(context, "开始拖动，触发位置：$offset", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDragStopped = { velocity ->
                        Toast
                            .makeText(context, "拖动结束，速度：$velocity", Toast.LENGTH_SHORT)
                            .show()
                    }
                )
        )
        VerticalSpacer()
        Text(text = "垂直拖动事件：")
        VerticalSpacer()
        var offsetY by remember { mutableStateOf(0f) }
        Box(
            modifier = Modifier
                .offset { IntOffset(0, offsetY.roundToInt()) }
                .background(Color.Yellow)
                .size(45.dp)
                .draggable(
                    state = rememberDraggableState(
                        onDelta = {
                            offsetY += it
                        }
                    ),
                    orientation = Orientation.Vertical,
                    onDragStarted = { offset ->
                        Toast
                            .makeText(context, "开始拖动，触发位置：$offset", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDragStopped = { velocity ->
                        Toast
                            .makeText(context, "拖动结束，速度：$velocity", Toast.LENGTH_SHORT)
                            .show()
                    }
                )
        )
    }
}